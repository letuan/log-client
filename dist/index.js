'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
function Log() {};

var _loop = function _loop(key) {
  if (console.hasOwnProperty(key) && typeof console[key] === 'function') {
    Log.prototype[key] = function () {
      if (process.env.NODE_ENV !== 'production') {
        console[key].apply(console, arguments);
      }
    };
  }
};

for (var key in console) {
  _loop(key);
}

exports.default = new Log();