function Log() {
	
};

for (let key in console) {
  if (console.hasOwnProperty(key) && typeof console[key] === 'function') {
  	Log.prototype[key] = function () {
  		if (process.env.NODE_ENV !== 'production') {
  			console[key].apply(console, arguments);
  		}
  	}
  }
}

export default new Log();

